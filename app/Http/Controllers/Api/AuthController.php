<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Enums\ErrorType;
use Illuminate\Http\Request;

class AuthController extends ApiController
{
    private $guard;
    public function __construct()
    {
        $this->guard = auth()->guard('api');
    }

    /**
     * SignIn user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function signIn(Request $request)
    {
        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        if (! $token = $this->guard->attempt($credentials)) {
            return $this->sendError(ErrorType::CODE_4011, ErrorType::STATUS_4011);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Refresh token
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken()
    {
        return $this->respondWithToken($this->guard->refresh());
    }

    public function signUp()
    {

    }

    public function account()
    {

    }

    private function respondWithToken($token)
    {
        return $this->sendSuccess([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard->factory()->getTTL() * 60
        ]);
    }
}
