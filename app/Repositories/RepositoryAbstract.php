<?php

declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

abstract class RepositoryAbstract implements RepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct()
    {
        $this->makeModel();
    }

    /**
     * @return Model
     */
    public function makeModel()
    {
        $this->model = app()->make($this->model());
    }

    /**
     * @return mixed
     */
    abstract function model();

    /**
     * @return Builder
     */
    public function query()
    {
        return $this->model->query();
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->model->all();
    }

    public function get(array $columns = ['*'])
    {
        return $this->model->select($columns)->get();
    }

    /**
     * @param int $id, array $attributes
     * @return array
     */
    public function find(int $id, array $attributes = null)
    {
        if ($attributes) {
            $result = $this->model::select($attributes)->findOrFail($id);
        } else {
            $result = $this->model->findOrFail($id);
        }

        return $result;
    }

    /**
     * @param array $data
     * @param int|null $id
     * @return mixed
     */
    public function save(array $data, int $id = null)
    {
        return $this->model->updateOrCreate(['id' => $id], $data);
    }

    /**
     * @param array $ids
     * @return int
     */
    public function destroy(array $ids)
    {
        return $this->model->destroy($ids);
    }
}
