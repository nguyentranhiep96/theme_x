<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $models = [
            'User'
        ];
        foreach ($models as $model) {
            $this->app->singleton(
                "App\Repositories\{$model}RepositoryInterface",
                "App\Repositories\{$model}Repository"
            );
        }

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
