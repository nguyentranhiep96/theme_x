import {extend} from "vee-validate";
import * as rules from "vee-validate/dist/rules";
import {LANGUAGE_DEFAULT} from "@/commons/constants";

export async function loadVeeValidate(locale: string) {
  if (locale === LANGUAGE_DEFAULT) locale = LANGUAGE_DEFAULT;
  const { messages } = await import(`vee-validate/dist/locale/${locale}.json`);
  Object.keys(rules).forEach(rule => {
    extend(rule, {
      ...rules[rule], // copies rule configuration
      message: messages[rule] // assign message
    });
  });
}
