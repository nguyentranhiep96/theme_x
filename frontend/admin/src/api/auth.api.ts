import request from "@/utils/request"
export const authLogin = (data: any) => {
  return request({
    url: "/sign-in",
    method: "post",
    data
  })
}
