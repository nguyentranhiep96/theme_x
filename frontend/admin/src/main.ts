import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./plugins/i18n";
import 'jquery/src/jquery.js'
import 'bootstrap/dist/js/bootstrap.min.js'
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
Vue.use(BootstrapVue, IconsPlugin);

import MainLayout from "./layouts/MainLayout.vue";
import GuestLayout from "./layouts/GuestLayout.vue";

Vue.component("MainLayout", MainLayout);
Vue.component("GuestLayout", GuestLayout);

Vue.config.productionTip = false;
Vue.config.performance = true;
Vue.prototype.$log = console.log.bind(console);

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
