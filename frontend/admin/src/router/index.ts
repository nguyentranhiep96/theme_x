import Vue from "vue";
import VueRouter, {RouteConfig} from "vue-router";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    meta: {
      layout: "GuestLayout"
    },
    component: {
      render(c) {
        return c("router-view");
      }
    },
    children: [
      {
        path: "/login",
        name: "Login",
        meta: {
          layout: "GuestLayout"
        },
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/Auth/Login.vue")
      }
    ]
  },
  {
    path: "/",
    meta: {
      layout: "MainLayout"
    },
    component: {
      render(c) {
        return c("router-view");
      }
    },
    children: [
      {
        path: "",
        name: "Home",
        meta: {
          layout: "MainLayout"
        },
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/Home/Home.vue")
      },
      {
        path: "/messages",
        name: "Message",
        meta: {
          layout: "MainLayout"
        },
        component: () =>
          import(/* webpackChunkName: "about" */ "../views/Message/Message.vue")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
