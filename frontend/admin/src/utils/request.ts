import axios from "axios";

const service = axios.create({
  baseURL: "http://localhost/api",
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
    "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
  },
  timeout: 30000 * 4
});

service.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    console.log(error);
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => response.data,
  error => {
    console.log("err" + error);
    return Promise.reject(error.response.data);
  }
);

export default service;
