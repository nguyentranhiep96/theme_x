import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators"
import { authLogin } from "@/api/auth.api";

@Module({ namespaced: true })
class Auth extends VuexModule {
  public accessToken: any = null;

  get token(): any {
    return this.accessToken;
  }

  @Mutation
  public setToken(token: any): void {
    this.accessToken = token;
  }

  @Action
  public login(data: any): Promise<any>  {
    return authLogin(data).then(response => {
      this.context.commit("setToken", response.data.access_token);
      return Promise.resolve(response);
    }).catch(error => {
      return Promise.reject(error);
    })
  }
}

export default Auth;
